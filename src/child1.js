import React from 'react';
import Child2 from './child2.js';


class Child1 extends React.Component {
   constructor(props) {
      super(props);
      
      this.state = {
         data: '',
         age : '',
         mobile:''
         
      }
      this.updateState = this.updateState.bind(this);
       };
          
      updateState(e) {
        this.setState({
              [e.target.name]: e.target.value
            });
   }


  render() 
          
{

      return (
         <div>
            <h4> <label> NAME  : {this.state.name}</label><br/></h4>
            <input type = "text" name='data' value = {this.props.data} 
           onChange = {this.updateState} />
          <h4> <label> AGE : {this.state.age}</label></h4>
            <input type = "text" name='age' value = {this.props.age} 
           onChange = {this.updateState} />
          <h4>  <label> MOBILE: {this.state.mobile}</label></h4>
           <input type = "text" name='mobile' value = {this.props.mobile} 
           onChange = {this.updateState} />
           <hr/>
           <Child2 namefrom={this.state.data} agefrom={this.state.age} mobilefrom={this.state.mobile}/>
           <hr/>
            </div>
         
        
         
      );

   }
}

  
export default Child1;