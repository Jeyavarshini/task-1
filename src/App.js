import React, { Component } from 'react';
import './App.css';
import Child1 from './child1.js';

class App extends Component {
  render() {
    return (
      <div className="App">
      <Child1/>
      </div>
    );
  }
}

export default App;
